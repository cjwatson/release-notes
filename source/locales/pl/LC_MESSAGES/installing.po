# Copyright (C) 2012 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# Michał Kułach <michal.kulach@gmail.com>, 2012, 2013, 2015, 2017.
msgid ""
msgstr ""
"Project-Id-Version:  \n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2017-06-15 13:23+0200\n"
"Last-Translator: Michał Kułach <michal.kulach@gmail.com>\n"
"Language: pl\n"
"Language-Team: Polish <debian-l10n-polish@lists.debian.org>\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && "
"(n%100<10 || n%100>=20) ? 1 : 2)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../installing.rst:4
msgid "Installation System"
msgstr "System instalacyjny"

#: ../installing.rst:6
msgid ""
"The Debian Installer is the official installation system for Debian. It "
"offers a variety of installation methods. The methods that are available "
"to install your system depend on its architecture."
msgstr ""
"Instalator Debiana (Debian Installer) jest oficjalnym systemem instalacji"
" w przypadku Debiana. Oferuje on wiele metod instalacji. To, które z nich"
" są dostępne, zależy od używanej architektury."

#: ../installing.rst:10
#, fuzzy
msgid ""
"Images of the installer for |RELEASENAME| can be found together with the "
"Installation Guide on the Debian website (|URL-INSTALLER|)."
msgstr ""
"Obrazy instalatora wydania |RELEASENAME| są dostępne razem z "
"przewodnikiem po instalacji na stronach Debiana (|URL-INSTALLER|)."

# | msgid ""
# | "The Installation Guide is also included on the first CD/DVD of the "
# | "official Debian CD/DVD sets, at:"
#: ../installing.rst:13
#, fuzzy
msgid ""
"The Installation Guide is also included on the first media of the "
"official Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Przewodnik po instalacji jest również dołączony do pierwszego CD/DVD "
"oficjalnego zestawu CD/DVD Debiana w:"

#: ../installing.rst:20
#, fuzzy
msgid ""
"You may also want to check the errata for debian-installer at |URL-"
"INSTALLER-ERRATA| for a list of known issues."
msgstr ""
"Można również zapoznać się z |URL-"
"INSTALLER-ERRATA| programu debian-installer, aby "
"poznać listę znanych błędów."

#: ../installing.rst:26
msgid "What's new in the installation system?"
msgstr "Co nowego w systemie instalacyjnym?"

# | msgid ""
# | "There has been a lot of development on the Debian Installer since its "
# | "previous official release with Debian |OLDRELEASE|, resulting in both "
# | "improved hardware support and some exciting new features."
#: ../installing.rst:28
#, fuzzy
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with Debian |OLDRELEASE|, resulting in improved"
" hardware support and some exciting new features or improvements."
msgstr ""
"W instalator Debiana włożono wiele pracy od jego ostatniego oficjalnego "
"wydania wraz z dystrybucją Debian |OLDRELEASE| dzięki czemu zwiększyła "
"się obsługa sprzętu oraz dodano wiele nowych, interesujących funkcji."

#: ../installing.rst:32
#, fuzzy
msgid ""
"If you are interested in an overview of the changes since "
"|OLDRELEASENAME|, please check the release announcements for the "
"|RELEASENAME| beta and RC releases available from the Debian Installer's "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."
msgstr ""
"W niniejszych uwagach do wydania zamieszczamy jedynie główne zmiany w "
"instalatorze. Szczegółowy przegląd zmian od chwili wydania |OLDRELEASENAME| "
"znajduje w ogłoszeniach wydań beta i RC dystrybucji |RELEASENAME|, w "
"`archiwalnych wiadomościach <https://www.debian.org/devel/debian-installer/News/>`__ "
"instalatora Debiana."

#: ../installing.rst:41
msgid "Something"
msgstr ""

#: ../installing.rst:43
msgid "Text"
msgstr ""

# | msgid "Automated installation"
#: ../installing.rst:48
#, fuzzy
msgid "Cloud installations"
msgstr "Instalacja automatyczna"

#: ../installing.rst:50
msgid ""
"The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian"
" |RELEASENAME| for several popular cloud computing services including:"
msgstr ""

#: ../installing.rst:53
msgid "Amazon Web Services"
msgstr ""

#: ../installing.rst:55
msgid "Microsoft Azure"
msgstr ""

#: ../installing.rst:57
msgid "OpenStack"
msgstr ""

#: ../installing.rst:59
msgid "Plain VM"
msgstr ""

#: ../installing.rst:61
msgid ""
"Cloud images provide automation hooks via ``cloud-init`` and prioritize "
"fast instance startup using specifically optimized kernel packages and "
"grub configurations. Images supporting different architectures are "
"provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""

#: ../installing.rst:67
msgid ""
"The cloud team will provide updated images until the end of the LTS "
"period for |RELEASENAME|. New images are typically released for each "
"point release and after security fixes for critical packages. The cloud "
"team's full support policy can be found `here "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."
msgstr ""

#: ../installing.rst:73
msgid ""
"More details are available at `<https://cloud.debian.org/>`__ and `on the"
" wiki <https://wiki.debian.org/Cloud/>`__."
msgstr ""

#: ../installing.rst:79
msgid "Container and Virtual Machine images"
msgstr ""

#: ../installing.rst:81
msgid ""
"Multi-architecture Debian |RELEASENAME| container images are available on"
" `Docker Hub <https://hub.docker.com/_/debian>`__. In addition to the "
"standard images, a \"slim\" variant is available that reduces disk usage."
msgstr ""

#: ../installing.rst:86
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published"
" to `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
msgstr ""
