# translation of installing debconf to Portuguese
# Portuguese translation for Debian's release-notes
# 2006-11-06 - Rui Branco <ruipb@debianpt.org> - initial translation
# Released under the same license as the release notes.
#
# Rui Branco <ruipb@debianpt.org>, 2006-2007.
# Ricardo Silva <ardoric@gmail.com>, 2007.
# Miguel Figueiredo <elmig@debianpt.org>, 2007.
# Carlos Lisboa <carloslisboa@gmail.com>, 2009.
# Pedro Ribeiro <p.m42.ribeiro@gmail.com>, 2011
# Miguel Figueiredo <elmig@debianpt.org>, 2012-2023.
msgid ""
msgstr ""
"Project-Id-Version:  installing\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-10-06 00:00+0200\n"
"PO-Revision-Date: 2023-05-30 21:12+0100\n"
"Last-Translator: Miguel Figueiredo <elmig@debianpt.org>\n"
"Language: pt\n"
"Language-Team: Portuguese <traduz@debianpt.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

#: ../installing.rst:4
msgid "Installation System"
msgstr "Sistema de Instalação"

#: ../installing.rst:6
msgid ""
"The Debian Installer is the official installation system for Debian. It "
"offers a variety of installation methods. The methods that are available "
"to install your system depend on its architecture."
msgstr ""
"O Instalador Debian é o sistema oficial de instalação da Debian. Oferece "
"uma variedade de métodos de instalação. Os métodos que estão disponíveis "
"para instalar o seu sistema dependem da arquitectura."

#: ../installing.rst:10
msgid ""
"Images of the installer for |RELEASENAME| can be found together with the "
"Installation Guide on the Debian website (|URL-INSTALLER|)."
msgstr ""
"As imagens do instalador para a |RELEASENAME| podem ser encontradas "
"juntamente com o Guia de Instalação no website Debian (|URL-INSTALLER|)."

#: ../installing.rst:13
msgid ""
"The Installation Guide is also included on the first media of the "
"official Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"O Guia de Instalação também está incluído no primeiro disco dos conjuntos"
" de DVDs (CD/blu-ray) Debian oficiais, em "

#: ../installing.rst:20
#, fuzzy
msgid ""
"You may also want to check the errata for debian-installer at |URL-"
"INSTALLER-ERRATA| for a list of known issues."
msgstr ""
"Pode também querer verificar a errata (at |URL-"
"INSTALLER-ERRATA|) do debian-installer para ver uma "
"lista de problemas conhecidos."

#: ../installing.rst:26
msgid "What's new in the installation system?"
msgstr "O que há de novo no sistema de instalação?"

#: ../installing.rst:28
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with Debian |OLDRELEASE|, resulting in improved"
" hardware support and some exciting new features or improvements."
msgstr ""
"Houve muito desenvolvimento no Instalador Debian desde o seu anterior "
"lançamento oficial com Debian |OLDRELEASE|, resultando em melhorias no "
"suporte de hardware e em algumas novas excitantes funcionalidades ou "
"melhorias."

#: ../installing.rst:32
msgid ""
"If you are interested in an overview of the changes since "
"|OLDRELEASENAME|, please check the release announcements for the "
"|RELEASENAME| beta and RC releases available from the Debian Installer's "
"`news history <https://www.debian.org/devel/debian-installer/News/>`__."
msgstr ""
"Se estiver interessado numa visão global das alterações desde "
"|OLDRELEASENAME|, por favor verifique os anúncios dos lançamentos beta e  RC "
"de |RELEASENAME| que estão disponíveis a partir do `histórico de notícias "
"<https://www.debian.org/devel/debian-installer/News/>`__ do Instalador Debian."

#: ../installing.rst:41
msgid "Something"
msgstr "Alguma coisa"

#: ../installing.rst:43
msgid "Text"
msgstr "Texto"

#: ../installing.rst:48
msgid "Cloud installations"
msgstr "Instalações na Cloud"

#: ../installing.rst:50
msgid ""
"The `cloud team <https://wiki.debian.org/Teams/Cloud>`__ publishes Debian"
" |RELEASENAME| for several popular cloud computing services including:"
msgstr ""
"A `equipa da cloud <https://wiki.debian.org/Teams/Cloud>`__ publica Debian "
"|RELEASENAME| para vários serviços de computação na cloud, incluindo:"

#: ../installing.rst:53
msgid "Amazon Web Services"
msgstr "Amazon Web Services"

#: ../installing.rst:55
msgid "Microsoft Azure"
msgstr "Microsoft Azure"

#: ../installing.rst:57
msgid "OpenStack"
msgstr "OpenStack"

#: ../installing.rst:59
msgid "Plain VM"
msgstr "VM Simples"

#: ../installing.rst:61
msgid ""
"Cloud images provide automation hooks via ``cloud-init`` and prioritize "
"fast instance startup using specifically optimized kernel packages and "
"grub configurations. Images supporting different architectures are "
"provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""
"As imagens para a cloud disponibilizam hooks para automação através de "
"``cloud-init`` e priorizam o arranque rápido das "
"instâncias utilizando pacotes de kernel e configurações de grub "
"especificamente optimizados para o efeito. As imagens que suportam "
"diferentes arquitecturas são disponibilizdas quando é apropriado e a "
"equipa da cloud esforça-se por suportar todas as funcionalidades "
"oferecidas pelo serviço da cloud."

#: ../installing.rst:67
msgid ""
"The cloud team will provide updated images until the end of the LTS "
"period for |RELEASENAME|. New images are typically released for each "
"point release and after security fixes for critical packages. The cloud "
"team's full support policy can be found `here "
"<https://wiki.debian.org/Cloud/ImageLifecycle>`__."
msgstr ""
"A equipa da cloud irá disponibilizar imagens atualizadas até ao fim do "
"período de LTS para |RELEASENAME|. As novas imagens são lançadas "
"tipicamente para cada lançamento pontual e após correções de segurança em"
" pacotes críticos. A política completa de suporte da equipa da cloud pode"
" ser encontrada `aqui <https://wiki.debian.org/Cloud/ImageLifecycle>`__."

#: ../installing.rst:73
msgid ""
"More details are available at `<https://cloud.debian.org/>`__ and `on the"
" wiki <https://wiki.debian.org/Cloud/>`__."
msgstr ""
"Estão disponíveis mais detalhes em `<https://cloud.debian.org/>`__ e `no "
"wiki <https://wiki.debian.org/Cloud/>`__."

#: ../installing.rst:79
msgid "Container and Virtual Machine images"
msgstr "Imagens de Container e de Máquina Virtual"

#: ../installing.rst:81
msgid ""
"Multi-architecture Debian |RELEASENAME| container images are available on"
" `Docker Hub <https://hub.docker.com/_/debian>`__. In addition to the "
"standard images, a \"slim\" variant is available that reduces disk usage."
msgstr ""
"Estão disponíveis imagens de container de Debian |RELEASENAME| de multi-"
"arquitectura no `Docker Hub <https://hub.docker.com/_/debian>`__. Além das "
"imagens standard, está disponível uma variante \"slim\" que reduz "
"a utilização do disco."

#: ../installing.rst:86
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published"
" to `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
msgstr ""
"As imagens de máquina virtual para Hashicorp Vagrant VM manager estão "
"publicadas na `Vagrant Cloud <https://app.vagrantup.com/debian>`__."
